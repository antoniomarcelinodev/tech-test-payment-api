using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace desafio_api.Entities
{
    public class Venda
    {
        public int Id { get; set; }
        public DateTime Data { get; set; }
        public string Itens { get; set; }
        public string Status { get; set; }

        [ForeignKey("Vendedor")]
        public string IdVendedor { get; set; }


    }
}