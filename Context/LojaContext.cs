using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using desafio_api.Entities;
using Microsoft.EntityFrameworkCore;

namespace desafio_api.Context
{
    public class LojaContext : DbContext
    {
        public LojaContext(DbContextOptions<LojaContext> options) : base(options){}

        public DbSet<Venda> Vendas { get; set; }
        public DbSet<Vendedor> Vendedores { get; set; }

    }
}