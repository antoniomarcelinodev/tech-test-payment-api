using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using desafio_api.Context;
using desafio_api.Entities;
using Microsoft.AspNetCore.Mvc;

namespace desafio_api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class VendaController : ControllerBase
    {
        private readonly LojaContext _context;

        public VendaController(LojaContext context)
        {
            _context = context;
        }


        [HttpPost]
        public IActionResult Registrar(Venda venda)
        {
            _context.Add(venda);
            _context.SaveChanges();
            return Ok(venda);
        }

        [HttpPut]
        public IActionResult Atualizar(int id, Venda venda)
        {
            var VendaBanco = _context.Vendas.Find(id);
            if(VendaBanco == null) return NotFound();
            VendaBanco.Status = venda.Status;
            
            _context.Vendas.Update(VendaBanco);
            _context.SaveChanges();
            return Ok(VendaBanco);
        }

        [HttpGet("{id}")]
        public IActionResult Buscar(int id)
        {
            var venda = _context.Vendas.Find(id);
            if(venda == null) return NotFound();
            return Ok(venda);
        }

       


    }
}